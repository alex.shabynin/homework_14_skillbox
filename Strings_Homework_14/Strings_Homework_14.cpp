// Strings_Homework_14.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
using namespace std;

int main()
{
    string myprint = "blablaboom and nightmare";
    size_t first = myprint.find_first_of(myprint);
    size_t last = myprint.find_last_of(myprint);
    
    cout << "The text is: " << myprint << endl;
    cout << "Length of string: " << myprint.length() << endl;
    cout << "First letter is: " << myprint [first] << endl;
    cout << "Last letter is: " << myprint[last] << endl;
        
    }


